"""
URL configuration for scrumptiousv3 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("recipes.urls")),
]


# In the same way that we installed the Django app in the Django project in the INSTALLED_APPS list, we have to add the urls.py to the project's urls.py.
# The urls.py in the scrumptious directory is the place where we register our other urls.py.
# Finally, we have to actually include those URLs. In the existing urlpatterns list, add a new line and type in the highlighted code:
