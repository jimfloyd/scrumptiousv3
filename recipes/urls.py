from django.urls import path
from recipes.views import show_recipe

urlpatterns = [
    path("recipes/", show_recipe),
]


# Now that we have a function Django can use to respond to HTTP requests, we have to configure Django to know about it! That's where the urls.py files come in.
# The first thing we'll do is import the path function from django.urls. This path function is the way that we associate a URL path with the function we just wrote.
# Finally, Django expects us to put all of our path-function registrations into a list. The variable that it wants us to use is urlpatterns, which, let's face it, is a bad variable name.
# But inside that list, the path function says that we're going to associate the URL path "recipes/" with the show_recipe function.
# The urls.py in the recipes directory is for registering our view functions. 
