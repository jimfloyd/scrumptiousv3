from django.shortcuts import render

# Create your views here.
def show_recipe(request):
    return render(request, "recipes/detail.html")
# All view functions take at least one parameter. The first parameter is the object that contains all of the HTTP request data in it.
# All view functions need to return some kind of HTTP response
# That render function always takes the request object as its first argument. The second argument is the path to our HTML file that Django will send back to the browser.

